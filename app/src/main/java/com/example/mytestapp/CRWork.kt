package com.example.mytestapp

import android.util.Log
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class CRWork {

    private val TAG = "CRWork"

    suspend fun simpleFlow(): Flow<Int> = flow {
        log(TAG, "simpleFlow started")
        for (i in 1..3) {
            delay(3000)
            emit(i)
        }
    }

    suspend fun networkApiCall(): Result {
        delay(3000)
        return Result(5)
    }

    companion object {
        fun log(tag: String, msg: String) = Log.i(tag, "[${Thread.currentThread().name}] $msg")
    }
}

data class Result(val value: Int)