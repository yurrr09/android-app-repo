package com.example.mytestapp

import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.*
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CrInstrumentedTest {

    @get: Rule
    val crScope = CRTestRule()

    private val TAG = "CRTEST"
    private fun log(msg: String) = CRWork.log(TAG, msg)
    private lateinit var subject: CRWork


    @Before
    fun setup() {
        subject = CRWork()
    }

    @After
    fun cleanup() {

    }

    @ExperimentalCoroutinesApi
    @Test
    fun fakeSubSuccessfulAndEmitsExpectedValues() = runBlockingTest {
        //Subscribe to trips booked notifications
        log("test START>>>")
        launch {
            delay(3000)
            val flowResult = subject.simpleFlow().first()
            Assert.assertEquals(flowResult, 1)
            log("received sf value $flowResult")
        }

        launch {
            val apiCallResult = subject.networkApiCall()
            Assert.assertEquals(apiCallResult.value, 5)
            log("received apiCallResult $apiCallResult")
        }
        //println("Sub ERROR -> ${(result as GqlResult.Failure).error.msg} ")
//        launch {
//            log("CALL MUTATION BOOKTRIP ")
//            val bookingResult = gqlClient.doMutation(TBookTripMutation(id = "84"))
//            Assert.assertTrue(bookingResult is GqlResult.Success)
//            Assert.assertEquals((bookingResult as GqlResult.Success).data.bookTrips.success, true)
//            Assert.assertEquals(bookingResult.data.bookTrips.launches?.first()?.id, "84")
//            log(" MUTATION BOOKTRIP SUCCESS ")
//        }


        //Assert.assertEquals(sub, 1)
        log("test END<<<")
//        val resultTestFlow = flowOf(1).first()
//        Assert.assertEquals(resultTestFlow, 1)

    }
}